function [ path_set ] = create_giant_pathset( f_dyn, x0, u_max, u_min, u_res, time_sample, dt, metadata)
%UNTITLED Returns a giant pathset by sampling control inputs and
%integrating over time periods specifed by time_sample
%   f_dyn is a function handle for dynamics. x0 is the origin of the
%   pathset tree. u_max and u_min are the max and min control inputs.
%   u_res is the sampling resolution of the control. time_sample is the
%   time steps that we integrate over. dt is the sample time. metadata is
%   an empty traj struct whose fields are to be emulated.

path_set = [];

if (length(fieldnames(metadata)) ~= length(x0))
    error('metadata doesnt match state size')
end

if (length(time_sample)==1)
    return;
end

for u = u_min:u_res:u_max
    [T, first_part] = ode45(@(t, x) f_dyn(x, u), time_sample(1):dt:time_sample(2), x0);
    second_set = create_giant_pathset( f_dyn, first_part(end, :), u_max, u_min, u_res, time_sample(2:end), dt, metadata);
    if (isempty(second_set))
        path = struct(metadata);
        field_list = fieldnames(path);
        for j = 1:length(field_list)
            path.(field_list{j}) = first_part(:,j)';
        end
        path_set = [path_set path];
    else
        for i = 1:length(second_set)
            path = struct(metadata);
            field_list = fieldnames(path);
            for j = 1:length(field_list)
                path.(field_list{j}) = [first_part(:,j)' second_set(i).(field_list{j})];
            end
            path_set = [path_set path];
        end
    end
end

end


