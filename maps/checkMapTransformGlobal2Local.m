function checkMapTransformGlobal2Local(minX,minY,maxX,maxY,resolution,translation,rotationAngle,bigMapResolution,bigMapMinX,bigMapMinY,bigMap)
%CHECKMAPTRANSFORMGLOBAL2LOCAL Summary of this function goes here
%   Detailed explanation goes here
%% extract localMap
localMap = findGlobal2LocalTransformedMap(minX,minY,maxX,maxY,resolution,translation,rotationAngle,bigMapResolution,bigMapMinX,bigMapMinY,bigMap);

%% draw GlobalMap and local Map rectangle and origin
localExtrema = [minX,minY;minX,maxY;maxX,maxY;maxX,minY;minX,minY];
globalExtrema = transformPoint2Global(localExtrema,rotationAngle,translation);
globalExtrema = coord2index(globalExtrema(:,1),globalExtrema(:,2),resolution,bigMapMinX,bigMapMinY);
figure
imshow(bigMap)
hold on
plot(globalExtrema(:,2),globalExtrema(:,1),'r');
%% draw localMap in a seperate window
figure
imshow(localMap)

end

