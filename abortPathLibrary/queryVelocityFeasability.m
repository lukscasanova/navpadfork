function [ validPaths ] = queryVelocityFeasability(map,stateMatrix,pathMap,queryState,minState,stateResolution,minX,minY,mapRes)

vId = value2Id(queryState,minState,stateResolution);
qPathIds = stateMatrix(vId(1),vId(2),vId(3)).pathIds;
bb = stateMatrix(vId(1),vId(2),vId(3)).boundingBox;
bb = coord2index(bb(:,1),bb(:,2),mapRes,minX,minY);

[relevantIDx,relevantIDy] = meshgrid(bb(1,1):bb(2,1),bb(1,2):bb(2,2));
relevantIDs = [relevantIDx(:),relevantIDy(:)];
relevantLinearID = sub2ind(size(map),relevantIDs(:,1),relevantIDs(:,2));
obsId = find(map(relevantLinearID)<= 0.5);

if(isempty(obsId))
    validPaths = qPathIds;
    return;
end
obstructedPathIds = [];

for i=1:size(obsId,1);
    obScalar = relevantIDs(obsId(i),1:2);
    if size(pathMap(obScalar(1),obScalar(2)).state(vId(1),vId(2),vId(3)).pathIds)>0
        obstructedPathIds = [obstructedPathIds;...
                   pathMap(obScalar(1),obScalar(2)).state(vId(1),vId(2),vId(3)).pathIds];
    end
end

[validPaths,~] = setdiff(qPathIds,obstructedPathIds,'rows');
end



