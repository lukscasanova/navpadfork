function wmap = randomRectangles(width, height, xticks, yticks, num_rect)
    wmap = ones(height, width);

    xs = [1, sort(randperm(width-2, xticks )+1), width];
    ys = [1, sort(randperm(height-2, yticks )+1), height];

    for i=1:num_rect
        x_ix = randi( numel(xs)-1 );
        y_ix = randi( numel(ys)-1 );

        x0 = xs(x_ix); x1 = xs(x_ix+1);
        y0 = ys(y_ix); y1 = ys(y_ix+1);

        wmap(y0:y1, x0:x1) = 0;
    end
    %wmap = imerode(wmap, strel('square', 5));
    %wmap = imerode(wmap, ones(5));
    
    wmap = imerode(wmap, ones(3));
    
    % add border
    wmap = padarray(wmap, [1 1], 0);
end
